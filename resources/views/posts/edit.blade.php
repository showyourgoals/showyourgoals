@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
             @endif


			@if (Session('success'))
			    <div class="alert alert-success" role="alert">
			        <strong>Successo:</strong> {{ session('success') }}
			    </div>
			@endif


            <h1>Modifica l'annuncio</h1>

            <hr>

 

            <form action="{{route('posts.update', [$post->id]) }}" method="POST" >
            	{{ csrf_field() }}
            	<input type="hidden" name="_method" value="PUT">

            	<div class="form-group">
            		<label for="title" class="control-label col-xs-12">Titolo:</label>
            		<div class="col-xs-12">
            			<input type="text" name="title" class="form-control" id="title" placeholder="Titolo" value="{{ old('title', $post->title)}}">
            		</div>
            	</div>

            	<div class="form-group"> 
            		<label for="description" class="control-label col-xs-12">Descrizione:</label>
            		<div class="col-xs-12">
            			<textarea class="form-control" id="description" name="description">
            				  {{ old('description', $post->description) }}
            			</textarea>
            		</div> 
            	</div> 

            	<button class="btn btn-success btn-lg btn-block">Memorizza</button>

            </form>
            <form action="{{ route('posts.destroy', [$post->id]) }}" method="POST">
            		{{ csrf_field() }}
            		<input type="hidden" name="_method" value="DELETE">
            		<button class="btn btn-danger btn-lg btn-block">Cancella</button>
            	</form>
        </div>
    </div>
</div>
@endsection
