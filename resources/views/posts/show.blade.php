@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
	    
	    @if (Session('modifiche'))
		    <div class="alert alert-success" role="alert">
		        <strong>Successo:</strong> {{ session('modifiche') }}
		    </div>
		@endif
		<div class="col-xs-12">
        	<h1 style="text-align: center;">Anteprima Post</h1>
        </div>	

    </div>

    <div class="row" style="margin-top: 50px;">
        <div class="col-xs-2">
	       		<h1><strong>Titolo:</strong></h1>
	    </div>
	    <div class="col-xs-10"><h1>{{ $post->title }}</h1></div>
	</div>

	<div class="row">
	    <div class="col-xs-2">
	        <h2><strong>Descrizione:</strong></h2>
	    </div>
   		<div class="col-xs-10">
	        <p style="font-size: 30px;">{{ $post->description }}</p>
	    </div>
	</div>
<div class="container well">
	<div class="row text-center">
		    <div class="col-xs-6">
		        <dl class="horizontal">
		        	<dt>Creato il:</dt>
		        	<dd>{{ date('M j, Y H:i', strtotime($post->created_at)) }}</dd>
		        </dl>
		    </div>	 
		    <div class="col-xs-6">
		        <dl class="horizontal">
		        	<dt>Ultima modifica:</dt>
		        	<dd>{{ date('M j, Y H:i', strtotime($post->updated_at)) }}</dd>
		        </dl>	
		    </div>	  
		        <hr>
		        <div class="row">
		        	<div class="col-sm-6">
		        		<a href="#" class="btn btn-primary btn-block">Modifica</a>
		        	</div>
		        	<div class="col-sm-6">
		        		<a href="#" class="btn btn-danger btn-block">Cancella</a>
		        	</div>
		        </div>
		 
    </div>
</div>

    <div class="row">
	    <div class="col">
	        <a href="{{route('home')}}" class="btn btn-lg btn-primary btn-block">Ritorna alla home</a>
        </div>
     </div>

  
</div>
@endsection
