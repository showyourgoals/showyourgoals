@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
             @endif
            <h1>Inserisci un nuovo annuncio</h1>

            <hr>
            <form action="{{route('posts.store') }}" method="POST" >
                {{ csrf_field() }}

                <input name="user_id" type="hidden" value="{{Auth::getUser()->id}}">

                <div class="form-group">
                    <label for="title" class="control-label col-xs-12">Titolo:</label>
                    <div class="col-xs-12">
                        <input type="text" name="title" class="form-control" id="title" placeholder="Titolo" value="{{ old('title', $post->title)}}">
                    </div>
                </div>

                <div class="form-group"> 
                    <label for="description" class="control-label col-xs-12">Descrizione:</label>
                    <div class="col-xs-12">
                        <textarea class="form-control" id="description" name="description">
                            {{ old('description', $post->description) }}
                        </textarea>
                    </div> 
                </div> 

                <button class="btn btn-success btn-lg btn-block" style="margin-top: 50px;">Memorizza</button>
                
            </form>
           

        </div>
    </div>
</div>
@endsection
