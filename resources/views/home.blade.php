@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    @if (Session('modifiche'))
                <div class="alert alert-success" role="alert">
                    <strong>Successo:</strong> {{ session('modifiche') }}
                </div>
            @endif
            
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

            

                <div class="panel-heading"><h2>BENVENUTO {{Auth::user()->name}}</h2></div>

                <div class="panel-body">

                    <h2>Questa è la tua home</h2>

                    <br>

                    <a href="{{route('posts.create')}}">Inserisci un aggiornamento su te stesso</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
