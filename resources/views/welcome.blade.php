@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                    Benvenuto in <i>SHOW YOUR GOALS!</i>
                    
                    <br>

                     <a href="{{route('home')}}">Home</a>

                     <br>

                    <a href="{{route('posts.create')}}">Inserisci un aggiornamento su te stesso</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
