<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Home pubblica 

Route::get('/', function () {
    return view('welcome');
});


//Utente registrato

Auth::routes();

Route::get('/home', 'PostController@index')->name('home');

Route::resource('posts', 'PostController');
